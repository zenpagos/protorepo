import click

from src.splash import print_splash
from src.grpc_generator import generate
from src.check_programs import check_programs


@click.group()
def main():
    pass


@main.command()
def build():
    print_splash()
    check_programs()
    generate()


if __name__ == "__main__":
    main()
