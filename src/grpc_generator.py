import os
import shutil
import ntpath
import click
import subprocess

from .param_fetcher import get_repositories, get_services
from .constants import (
    SERVICE_PROTO_FILE_KEY,
    SERVICE_LANGS_KEY,
    ARTIFACTS_PATH,
    REPOSITORY_URL,
    REPOSITORY_DESTINATION_FILES
)


def _get_path(proto_file):
    return os.path.dirname(os.path.realpath(proto_file))


def _generate_grpc_files(lang, proto_path, proto_file):
    file = ntpath.basename(proto_file)
    subprocess.call(
        ['docker', 'run', '-v', f'{proto_path}:/defs', 'namely/protoc-all', '-f', file, '-l', lang, '-o', lang])


def _gen_service_configurations(services, repositories):
    configuration = []

    for service in services:
        langs = service.get(SERVICE_LANGS_KEY)

        for lang in langs:
            service_conf = {}
            service_name = service.get('name')
            proto_file = service.get(SERVICE_PROTO_FILE_KEY)
            proto_path = _get_path(proto_file)
            grpc_path = f'{proto_path}/{lang}'

            service_conf['name'] = service_name
            service_conf['lang'] = lang
            service_conf['proto_file'] = proto_file
            service_conf['proto_path'] = proto_path
            service_conf['grpc_path'] = grpc_path

            for repository in repositories.items():
                if lang == repository[0]:
                    options = repository[1]
                    repository_path = f'{ARTIFACTS_PATH}/{lang}'
                    repository_destination_files = f'{repository_path}/{options.get(REPOSITORY_DESTINATION_FILES)}/{service_name}'

                    service_conf[REPOSITORY_URL] = options.get(REPOSITORY_URL)
                    service_conf['repository_path'] = repository_path
                    service_conf[REPOSITORY_DESTINATION_FILES] = repository_destination_files

            configuration.append(service_conf)

    return configuration


def _clone_repositories(repositories):
    for repository in repositories.items():
        lang = repository[0]
        options = repository[1]

        repository_url = options.get(REPOSITORY_URL)
        repository_path = f'{ARTIFACTS_PATH}/{lang}'

        subprocess.call(['git', 'clone', repository_url, repository_path])


def _delete_repositories(repositories):
    for repository in repositories.items():
        lang = repository[0]
        repository_path = f'{ARTIFACTS_PATH}/{lang}'

        _delete_dir(repository_path)


def _move_files_to_repos(lang, grpc_path, destination_files):
    files = os.listdir(grpc_path)

    for f in files:
        file_origin_path = os.path.join(grpc_path, f)
        file_destination_path = os.path.join(destination_files, f)

        # create folder in the repo if not exists
        if not os.path.exists(destination_files):
            os.makedirs(destination_files)

        # delete de old file from the repo if not exists
        if os.path.exists(file_destination_path):
            os.remove(file_destination_path)

        shutil.move(file_origin_path, destination_files)


def _delete_dir(proto_path):
    shutil.rmtree(path=proto_path, ignore_errors=True)


def _push_changes(repositories):
    for repository in repositories.items():
        lang = repository[0]
        work_tree = f'{ARTIFACTS_PATH}/{lang}'
        git_dir = f'{ARTIFACTS_PATH}/{lang}/.git/'

        subprocess.call(['git', f'--git-dir={git_dir}', f'--work-tree={work_tree}', 'add', '-A'])
        subprocess.call(['git', f'--git-dir={git_dir}', f'--work-tree={work_tree}', 'commit', '-m', 'auto commit'])
        subprocess.call(['git', f'--git-dir={git_dir}', f'--work-tree={work_tree}', 'push', 'origin', 'HEAD'])


def generate():
    services = get_services()
    repositories = get_repositories()
    service_configurations = _gen_service_configurations(services, repositories)

    os.mkdir(ARTIFACTS_PATH)

    click.secho('Cloning repositories', fg='cyan')
    _clone_repositories(repositories)

    click.secho('Generating gRPC files', fg='cyan')
    for sc in service_configurations:
        lang = sc.get('lang')
        name = sc.get('name')
        proto_path = sc.get('proto_path')
        proto_file = sc.get('proto_file')
        grpc_path = sc.get('grpc_path')
        destination_files = sc.get(REPOSITORY_DESTINATION_FILES)

        click.secho(f'Generating gRPC files for {lang}, service: {name}', fg='red')
        _generate_grpc_files(lang, proto_path, proto_file)

        click.secho(f'Moving generated files to repository', fg='red')
        _move_files_to_repos(lang, grpc_path, destination_files)

        click.secho(f'Deleting dir {grpc_path}', fg='red')
        _delete_dir(grpc_path)

    click.secho('Pushing changes to repositories', fg='cyan')
    _push_changes(repositories)

    _delete_dir(ARTIFACTS_PATH)
