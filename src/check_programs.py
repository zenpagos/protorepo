from shutil import which

import click
import emoji

from .param_fetcher import get_programs


def check_programs():
    click.secho('Checking required programs', fg='cyan')

    for program in get_programs():
        if which(program) is None:
            click.secho(emoji.emojize(f':fire: {program} is not installed'), fg='red')
            raise click.Abort

        click.secho(emoji.emojize(f':heavy_check_mark: {program}'), fg='green')
