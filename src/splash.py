from pyfiglet import Figlet

from .constants import CLI_NAME


def print_splash():
    f = Figlet(font='shadow')
    print(f.renderText(CLI_NAME))
