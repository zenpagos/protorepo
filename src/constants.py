CLI_NAME = 'Zenpagos'
PARAMETER_FILE = 'parameters.yaml'
ARTIFACTS_PATH = 'artifacts'

# parameters.yaml keys
REPOSITORIES_KEY = 'repositories'
REPOSITORY_URL = 'url'
REPOSITORY_DESTINATION_FILES = 'destination_files'
SERVICES_KEY = 'services'
PROGRAMS_KEY = 'programs'
SERVICE_PROTO_FILE_KEY = 'protobuf_file'
SERVICE_LANGS_KEY = 'langs'
