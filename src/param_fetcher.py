import yaml

from .constants import PARAMETER_FILE, REPOSITORIES_KEY, SERVICES_KEY, PROGRAMS_KEY


def _fetch_parameters():
    """ Load parameters from parameters.yaml file """
    try:
        stream = open(PARAMETER_FILE, 'r')
        return yaml.load(stream=stream,
                         Loader=yaml.FullLoader)
    except yaml.YAMLError as exc:
        print("Error in configuration file:", exc)


def get_programs():
    return _fetch_parameters().get(PROGRAMS_KEY)


def get_repositories():
    """ Gets repositories from parameters.yaml """
    return _fetch_parameters().get(REPOSITORIES_KEY)


def get_services():
    """ Gets services from parameters.yaml """
    return _fetch_parameters().get(SERVICES_KEY)
